package;

import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLUniformLocation;

import lime.math.Vector4;
import lime.math.Matrix4;

import lime.utils.Float32Array;
import lime.utils.Int32Array;
import lime.utils.DataPointer;
import lime.utils.BytePointer;


class Main extends Application {

    private var minX:Int;
    private var minY:Int;
    private var maxX:Int;
    private var maxY:Int;

    private var program : GLProgram;
    private var positionAttributeHandle : Int;

    private var vertexBufferHandleA : GLBuffer;
    private var vertexBufferHandleB : GLBuffer;
    private var elementBufferHandleB : GLBuffer;
    private var elementBufferPtrB1 : DataPointer;
    private var elementBufferPtrB2 : DataPointer;

    private var triangleColorUniform : GLUniformLocation;
    private var matrixUniform : GLUniformLocation;

    private var rotationMatrixTriangleA : Matrix4;
    private var rotationPointTriangleA : Vector4;
    private var identityMatrix : Matrix4;

    var triangle_B_buffer_data : Array<Float>;
    var triangle_B_indexes_data : Array<Int>;

    public function new () {

	    super ();

	    trace ("OpenGL Tutorial #9");
	    Log.info ("OpenGL Tutorial #9");
	}

    public override function onRenderContextLost ():Void {
	    trace ("onRenderContextLost");
	}

    public override function onRenderContextRestored (context:RenderContext):Void {
	    trace ("onRenderContextRestored");
	}

    public override function onPreloadComplete ():Void {
	    trace ("onPreloadComplete");
	    appInit();
	}

    private function appInit():Void {

	    window.width = 600;
	    window.height = 600;

	    minX = 0;
	    maxX = window.width;
	    minY = 0;
	    maxY = window.height;

	    init();


	    switch (window.context.type) {

		    case CAIRO:
			    Log.warn ("Render context not supported: CAIRO");

		    case CANVAS:
			    Log.warn ("Render context not supported: CANVAS");

		    case DOM:
			    Log.warn ("Render context not supported: DOM");

		    case FLASH:
			    Log.warn ("Render context not supported: FLASH");

		    case OPENGL, OPENGLES, WEBGL:

			    initShader(window.context);
			    initGL(window.context);

		    default:

			    Log.warn ("Current render context not supported by this sample");

		}
	}

    private function init():Void {
		// by default, both matrices are set to identity matrix
	    rotationMatrixTriangleA = new Matrix4();
	    identityMatrix = new Matrix4();

	    rotationPointTriangleA = new Vector4();
	}

    private function initShader (context:RenderContext):Void {

	    var gl = context.webgl;

	    var vertexShaderSource = "
			// #version 120
		    attribute vec3 position;
		    uniform mat4 transformMatrix;

		    void main (void) {
			    gl_Position = transformMatrix * vec4(position, 1.0);
			}
		";

		// we added 'triangleColor' uniform which is
		// going to serve as a way to use one shader program
		// for rendering triangles of different colours
	    var fragmentShaderSource = 
			#if (!desktop || rpi)
			"precision mediump float;" +
			#end
			"
			// #version 120
		    uniform vec3 triangleColor;

		    void main (void) {
			    gl_FragColor = vec4(triangleColor, 1.0);
			}
		";

	    program = GLProgram.fromSources (gl, vertexShaderSource, fragmentShaderSource);
		// NOT 'use'-ing ;) it here, we need to switch programs while we draw
		// so we'll call gl.useProgram() in render()
		// gl.useProgram (program);
	    positionAttributeHandle = gl.getAttribLocation( program, "position" );

        // grab program location of the uniform for triangle color
        triangleColorUniform = gl.getUniformLocation( program, "triangleColor" );
		// grab program location of the uniform for transformation matrix
	    matrixUniform = gl.getUniformLocation( program, "transformMatrix" );
	}

    private function initGL(context:RenderContext):Void {

	    var gl = context.webgl;


		// trace( "gl.version: " + gl.version );
		// trace( "gl.type: " + gl.type );
	#if (lime_opengl || lime_opengles)
	    trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
	    trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
	#end

		//
		// global GL context setup
		//
	    gl.clearColor(63.0/255.0, 127.0/255.0, 191.0/255.0, 1.0);  // Bluish
		// comment out this line to make triangle A appear behind triangle B when it is rotating
	    gl.enable(GL.DEPTH_TEST);
	    gl.clearDepth(1.0); // set clear depth value to farthest


		//
		// data setup
		//
        var triangle_A_buffer_data : Array<Float> = [
			// point 1
			-1.0, -1.0, -0.5, // NEGATIVE z-coordinate values means this triangle is CLOSER
			// point 2
		    0.0, -1.0, -0.5,
			// point 3
			-0.5,  1.0, -0.5,
		];
        zoom(triangle_A_buffer_data, 0.5);
		// we want rotation to happen around point 1 of triangle A
	    rotationPointTriangleA = new Vector4(
		    triangle_A_buffer_data[0],
		    triangle_A_buffer_data[1],
		    triangle_A_buffer_data[2],
		    0.0
		);
        vertexBufferHandleA = createAndBindVertexBuffer(gl, triangle_A_buffer_data);

        // var triangle_B_buffer_data : Array<Float> = [
        triangle_B_buffer_data = [
			// triangle B1
		    0.0, 1.0, 0.5, // POSITIVE z-coordinate values means this triangle is FARTHER
		    1.0, 1.0, 0.5,
		    0.5, -1.0, 0.5,

			// triangle B2
		    1.0, 1.0, 0.5,
		    0.5, -1.0, 0.5,
		    1.0, -1.0, 0.5,
		];
        triangle_B_indexes_data = [
			// triangle B1
		    0, 1, 2,

			// triangle B2
		    3, 4, 5,
		];
        zoom(triangle_B_buffer_data, 0.5);
        vertexBufferHandleB = createAndBindVertexBuffer(gl, triangle_B_buffer_data);

	    var bufferData:Int32Array = new Int32Array (triangle_B_indexes_data);
        elementBufferHandleB = createAndBindElementBuffer(gl, bufferData);

	    trace(bufferData);

		// var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(0,2));
		// var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(3,5));
		// var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(0));
		// var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(3));
	    var bytePointer = BytePointer.fromArrayBufferView (bufferData.subarray(0,3));
	    trace(bytePointer);
		// var elementBufferPtrB1 = DataPointer.fromBytesPointer(bytePointer);
	    var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(0,3));
	    trace(elementBufferPtrB1);

	    bytePointer = BytePointer.fromArrayBufferView (bufferData.subarray(3,6));
	    trace(bytePointer);
		// var elementBufferPtrB1 = DataPointer.fromBytesPointer(bytePointer);
	    var elementBufferPtrB1 = DataPointer.fromArrayBufferView(bufferData.subarray(3,6));
	    trace(elementBufferPtrB1);
	}

    public override function update (deltaTime:Int):Void {

	    if (!preloader.complete) return;

		// deltaTime is in milliseconds
	    var rotationSpeed = 90.0; // in degrees per second
	    var delta = deltaTime / 1000.0;

	    var rp = rotationPointTriangleA;
	    rotationMatrixTriangleA.appendTranslation( -rp.x, -rp.y, -rp.z );
	    rotationMatrixTriangleA.appendRotation( delta * rotationSpeed, Vector4.Z_AXIS );
	    rotationMatrixTriangleA.appendTranslation( rp.x, rp.y, rp.z );
	}

    public override function render (context:RenderContext):Void {

	    if (!preloader.complete) return;

	    switch (context.type) {

		    case OPENGL, OPENGLES, WEBGL:

			    var gl = context.webgl;

			    gl.viewport( 0, 0, maxX, maxY );
			    gl.clear( GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT );


				//
				// START
				//

				// calculate angle we will use to animate color and rotation
			    var period = 4000.0;
			    var theta = ( ( Sys.time() * 1000.0 ) % period ) / period;

				// Both triangles use the same program
				// "Acitvate" the 'position' attribute in our shader program
			    gl.enableVertexAttribArray ( positionAttributeHandle );
				// "Activate" our shader program
			    gl.useProgram( program );

				//
				// Triangle A
				//
				// set triangle A color
		        gl.uniform3f( triangleColorUniform, 0.5, 1.0, 0.0 );
				// Bind our triangle A vertex buffer to the current context
			    gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandleA );
				// "Describe" the attribute structure
			    gl.vertexAttribPointer( positionAttributeHandle, 3, GL.FLOAT, false, 0, 0 );
				// set transformation matric for triangle A
			    gl.uniformMatrix4fv( matrixUniform, false, rotationMatrixTriangleA );
				// Draw the triangle A
			    gl.drawArrays( GL.TRIANGLES, 0, 3 );
				// once done, disable 'position' attribute array so it doesn't conflict with next program, if any.
				// gl.disableVertexAttribArray( positionAttributeHandle );

				//
				// Triangle B
				//
			    gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandleB );
			    gl.bindBuffer( GL.ELEMENT_ARRAY_BUFFER, elementBufferHandleB );
			    gl.vertexAttribPointer( positionAttributeHandle, 3, GL.FLOAT, false, 0, 0 );
				// set transformation matrix for triangle B
			    gl.uniformMatrix4fv( matrixUniform, false, identityMatrix );
				//
				// B1
				//
				// set triangle B1 color
		        gl.uniform3f( triangleColorUniform, 0.5, 0.0, 0.0 );
			    gl.drawElements( GL.TRIANGLES, 3, GL.UNSIGNED_INT, elementBufferPtrB1 );
				//
				// B2
				//
				// let's animate triangle B2 color
				// calculate color value for red channel
				// these three lines will give us a number that changes smoothly from 0.0 to 1.0 and back.
				// the time, in milliseconds, it takes it to go from 0.0 to 1.0 is given by the variable 'period'
				// var period = 4000.0;
				// var theta = ( ( Sys.time() * 1000.0 ) % period ) / period;
			    var red = ( Math.sin( theta * Math.PI * 2 ) + 1.0 ) / 2.0;
		        gl.uniform3f( triangleColorUniform, red, 0.0, 1.0 );
			    gl.drawElements( GL.TRIANGLES, 3, GL.UNSIGNED_INT, elementBufferPtrB1 );

				//
				// END
				//
			    gl.disableVertexAttribArray( positionAttributeHandle );

		    default:

		}
	}

    private function createAndBindVertexBuffer( gl:WebGLRenderContext, a_buffer_data : Array<Float> ):GLBuffer {

	    var bufferData:Float32Array = new Float32Array (a_buffer_data);

        // transfer vertex data to GPU memory
        // this way we don't have to keep buffers in RAM and transfer them on every frame render

        // Generate 1 buffer, put the resulting identifier in vertexbufferHandles
	    var bufferHandle:GLBuffer = gl.createBuffer (); // == glGenBuffers( 1, &bufferHandle )

        // this sets the type of data we want to upload to GPU
        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ARRAY_BUFFER, bufferHandle );
	    gl.bindBuffer (GL.ARRAY_BUFFER, bufferHandle);


        // We are uploading vertex buffer (GL_ARRAY_BUFFER)
        // and the vertex data will be uploaded once and rendered many times (GL_STATIC_DRAW)
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame

        // This effectively uploads data to GPU memory
        // int numBytes = g_vertex_buffer_data.length * Float.BYTES;
        // gl.glBufferData(GL.GL_ARRAY_BUFFER, numBytes, bufferData, GL.GL_STATIC_DRAW);
	    gl.bufferData (GL.ARRAY_BUFFER, bufferData, GL.STATIC_DRAW);

	    return bufferHandle;
	}

    private function createAndBindElementBuffer( gl:WebGLRenderContext, bufferData:Int32Array ) : GLBuffer {

        // transfer vertex data to GPU memory
        // this way we don't have to keep buffers in RAM and transfer them on every frame render

        // Generate 1 buffer, put the resulting identifier in vertexbufferHandles
	    var bufferHandle:GLBuffer = gl.createBuffer (); // == glGenBuffers( 1, &bufferHandle )

        // this sets the type of data we want to upload to GPU
        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ELEMENT_ARRAY_BUFFER, bufferHandle );
	    gl.bindBuffer (GL.ELEMENT_ARRAY_BUFFER, bufferHandle);


        // We are uploading vertex buffer (GL_ARRAY_BUFFER)
        // and the vertex data will be uploaded once and rendered many times (GL_STATIC_DRAW)
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame

        // This effectively uploads data to GPU memory
        // int numBytes = g_vertex_buffer_data.length * Integer.BYTES;
        // gl.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, numBytes, bufferData, GL.GL_STATIC_DRAW);
	    gl.bufferData (GL.ARRAY_BUFFER, bufferData, GL.STATIC_DRAW);

	    return bufferHandle;
	}

    private function zoom( triangle_buffer_data : Array<Float>, factor:Float):Void {
	    for (i in 0...triangle_buffer_data.length) {
            triangle_buffer_data[ i ] *= factor;
        }
    }
}