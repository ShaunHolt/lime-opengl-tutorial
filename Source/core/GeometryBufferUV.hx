package core;

import lime.graphics.WebGLRenderContext;

import lime.utils.Float32Array;

import xgl.XGLArrayBuffer;


class GeometryBufferUV {

	public var buffer : XGLArrayBuffer;

    public function new( gl : WebGLRenderContext, bufferData : Array<Float> ) {

        // attribute format:
        // X, Y, Z, U, V

		buffer = new XGLArrayBuffer( gl, new Float32Array( bufferData ), 5 );
		buffer.addAttribute( 'position', 3, 0 * Float32Array.BYTES_PER_ELEMENT );
		buffer.addAttribute( 'aTextureUV', 2, 3 * Float32Array.BYTES_PER_ELEMENT );
    }
}
