package core;

import lime.math.Matrix4;


class Mesh {

    public var vertices : Array< Array< Float > >;
    public var uvs : Array< Array< Float > >;

    public var numberOfPrimitives (get, never) : Int;

    /** Value of this matrix is used to set a shader uniform **/
    public var transformMatrix : Matrix4;

    public function new() {

        vertices = [];
        uvs = [];

        transformMatrix = new Matrix4();
    }

    function get_numberOfPrimitives() return vertices.length;
}
