package core;

import lime.math.Matrix4;
import lime.math.Vector4;

import core.Mesh;
import core.Material;

import core.material.BasicMaterial;

class SceneObject {

    public var mesh : Mesh;
    public var material : Material;

    public function new() {

        mesh = new Mesh();
        material = new BasicMaterial();
    }

    public function translate( deltaVector : Vector4 ) : SceneObject {

        mesh.transformMatrix.appendTranslation( deltaVector.x, deltaVector.y, deltaVector.z );

        return this;
    }

    public function rotate( deltaAngleDegrees : Float, axis : Vector4 ) : SceneObject {

        mesh.transformMatrix.appendRotation( deltaAngleDegrees, axis );

        return this;
    }

    public function moveTo( position : Vector4 ) : SceneObject {

        mesh.transformMatrix.position = position;

        return this;
    }
}
