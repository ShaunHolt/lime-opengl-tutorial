package core.material;

import lime.graphics.Image;

import lime.math.RGBA;

import core.Material;


class BasicMaterial extends Material {

    public function new( color : RGBA = 0xFF00FFFF ) {

        super();

        emissiveColor = color;
    }
}
