package xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.math.RGBA;

import xgl.aux.XGLColor;


class XGLMaterial {

    // Texture object containing uploaded texture handle and other data
	public var texture : XGLTexture;

	// Material base color, a.k.a. emissive color, a.k.a. Ke
    public var color : XGLColor;

    public function new( gl : WebGLRenderContext, ?rgba : RGBA ) {

	    color = new XGLColor();
	    color.setFromRGBA( rgba != null ? rgba : 0xFFFFFFFF );
	}
}
