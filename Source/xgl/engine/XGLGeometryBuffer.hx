package xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.utils.Float32Array;

import xgl.engine.XGLArrayBuffer;

import xgl.engine.XGLShader;


class XGLGeometryBuffer {

	public var buffer : XGLArrayBuffer;
    public var shader : XGLShader;

    public function new( gl : WebGLRenderContext, shader : XGLShader ) {

        this.shader = shader;
    }

    public function describeProgramAttributes( gl : WebGLRenderContext ) : Void { }
}
