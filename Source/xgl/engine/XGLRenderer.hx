package xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import core.Scene;

import xgl.engine.XGLRenderObject;


class XGLRenderer {

    public function new( gl : WebGLRenderContext ) {

	    initGLState( gl );
	}

    private function initGLState( gl : WebGLRenderContext ) : Void {

		//
		// global GL context setup
		//

	    gl.clearColor(63.0/255.0, 127.0/255.0, 191.0/255.0, 1.0);  // Bluish
		// comment out this line to make triangle A appear behind triangle B when it is rotating
	    gl.enable( GL.DEPTH_TEST );
	    gl.clearDepth( 1.0 ); // set clear depth value to farthest

		#if desktop
	    gl.enable( GL.TEXTURE_2D );
		#end
	}

    public function render( gl : WebGLRenderContext, scene : Scene ) {

		scene.updateRenderObjectList( gl );
        var renderObjectList = scene.getRenderObjectList( gl );

		//
		// START
		//

		//
		// SETUP
		//
		// gl.viewport( 0, 0, maxX, maxY );
	    gl.clear( GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT );


		//
		// RENDER
		//
	    for( renderObject in renderObjectList ) {
		    renderObject.render( gl, scene );
		}
		// quadRenderObject.render( gl );
		// triangleRenderObject.render( gl );


		//
		// END
		//
	    gl.bindBuffer( GL.ARRAY_BUFFER, null );
	    gl.bindTexture( GL.TEXTURE_2D, null );
	}
}
