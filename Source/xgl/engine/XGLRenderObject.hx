package xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.math.RGBA;

import core.Mesh;
import core.Material;
import core.Scene;
import core.SceneObject;

import xgl.aux.XGLColor;


class XGLRenderObject {

	private var sceneObject : SceneObject;

    public var mesh : Mesh;
    public var meshMaterial : Material;

    public var material : XGLMaterial;
    public var geometryBuffer : XGLGeometryBuffer;
    public var shader : XGLShader;

	public function new( gl : WebGLRenderContext, sceneObject : SceneObject ) {

		this.sceneObject = sceneObject;

        mesh = sceneObject.mesh;
		meshMaterial = sceneObject.material;

		create( gl );
	}

	private function create( gl : WebGLRenderContext ) : Void { }

	public function render( gl : WebGLRenderContext, scene : Scene ) : Void {

		//
		// --- SETUP
		//

		//
		// ACTIVATE PROGRAM
		//
		gl.useProgram( shader.program.programHandle );

		//
		// Enable PROGRAM attributes
		//
		for( attr in shader.program.attributeHandles ) {
			gl.enableVertexAttribArray( attr );
		}


		//
		// --- RENDER
		//
		renderSetupGeometry( gl, scene );
		renderSetupMaterial( gl, scene );
		renderDraw( gl );



		//
		// --- CLEANUP
		//

		//
		// Disable PROGRAM attributes
		//
		for( attr in shader.program.attributeHandles ) {
			gl.disableVertexAttribArray( attr );
		}
	}

	private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void { }

	private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void { }
	
	private function renderDraw( gl : WebGLRenderContext ) : Void { }
}
