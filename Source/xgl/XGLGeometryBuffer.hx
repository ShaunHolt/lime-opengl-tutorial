package xgl;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GLBuffer;

import lime.utils.Float32Array;


class XGLGeometryBuffer {

	public var vertexBufferHandle : GLBuffer;
	public var geometryBufferData : Float32Array;

	// used for vertexAttribPointer
	public var vertexAttributesSize : Int;
	public var attributeParams : Map< String, XGLVertexAttributeParams >;
	// ------------------------------------------

	public function new( gl : WebGLRenderContext, bufferData : Float32Array, attributeSize : Int ) {

		attributeParams = [];
		geometryBufferData = bufferData;
		vertexAttributesSize = attributeSize * Float32Array.BYTES_PER_ELEMENT;

        vertexBufferHandle = XGLUtils.createAndUploadVertexBuffer( gl, geometryBufferData );
	}

	public function addAttribute( name : String, componentCount : Int, attributeOffset : Int ) : Void {

		attributeParams[ name ] = new XGLVertexAttributeParams( componentCount, attributeOffset );
	}
}
