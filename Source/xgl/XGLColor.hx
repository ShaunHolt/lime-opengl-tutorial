package xgl;

import lime.math.RGBA;


class XGLColor {

    public var r : Float;
    public var g : Float;
    public var b : Float;
    public var a : Float;

    public function new() {

	    r = g = b = a = 0.0;
	}

    public function setComponents( r : Float, g : Float, b : Float, a : Float ) : Void {

	    this.r = r < 0.0 ? 0.0 : r > 1.0 ? 1.0 : r;
	    this.g = g;
	    this.b = b;
	    this.a = a;
	}

    public function setFromRGBA( rgba : RGBA ) {

	    r = rgba.r / 255.0;
	    g = rgba.g / 255.0;
	    b = rgba.b / 255.0;
	    a = rgba.a / 255.0;
	}

    public static inline function create( r : Float, g : Float, b : Float, a : Float ) : XGLColor {

	    var xcolor = new XGLColor();

	    xcolor.setComponents( r, g, b, a );

	    return xcolor;
	}
}
