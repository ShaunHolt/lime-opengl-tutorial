package xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import xgl.XGLShaderProgram;
import xgl.XGLColor;

import xgl.material.XGLSolidColorMaterial;

import xgl.shader.XGLShader;
import xgl.shader.XGLSolidColorShader;

import xgl.geometry.XGLGeometryBuffer;
import xgl.geometry.XGLGeometryBufferBasic;

import xgl.render.XGLRenderObject;


class XGLTriangleRenderObject extends XGLRenderObject {

	public var material : XGLSolidColorMaterial;

	public var gb : XGLGeometryBuffer;
	public var shader : XGLShader;

	public function new( gl : WebGLRenderContext ) {

		super( gl );
	}

	override private function init( gl : WebGLRenderContext ) : Void {

		//
		// PROGRAM
		//

		shader = new XGLSolidColorShader( gl );
		shaderProgram = shader.program;

		//
		// GEOMETRY
		//

        var triangle_A_buffer_data : Array<Float> = [
			// vertex 0
			-0.5, -0.5, 0.0,
			// vertex 1
			-0.5, 0.5, 0.0,
			// vertex 2
			0.5, -0.5, 0.0,
		];

		gb = new XGLGeometryBufferBasic( gl, shader, triangle_A_buffer_data );

		//
		// MATERIAL
		//
		material = new XGLSolidColorMaterial( gl, 0x00FF8FFF );
	}

	override private function renderSetupMaterial( gl : WebGLRenderContext ) : Void {

		//
		// Set UNIFORM values
		//
		gl.uniform3f( shaderProgram.uniforms['triangleColor'], material.color.r, material.color.g, material.color.b );
	}

	override private function renderSetupGeometry( gl : WebGLRenderContext ) : Void {

		//
		// Setup ATTRIBUTE values
		//
		gl.bindBuffer( GL.ARRAY_BUFFER, gb.buffer.vertexBufferHandle );
		gb.describeProgramAttributes( gl );
	}
	
	override private function renderDraw( gl : WebGLRenderContext ) : Void {

		//
		// Do DRAWING
		//
		gl.drawArrays( GL.TRIANGLES, 0, 3 );
	}
}
