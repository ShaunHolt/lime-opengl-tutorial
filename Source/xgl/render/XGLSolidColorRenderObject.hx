package xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import core.Scene;

import xgl.aux.XGLColor;

import xgl.engine.XGLRenderObject;

import xgl.material.XGLSolidColorMaterial;

import xgl.geometry.XGLGeometryBufferBasic;

import xgl.shader.XGLSolidColorShader;


class XGLSolidColorRenderObject extends XGLRenderObject {

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // PROGRAM
        //

        shader = new XGLSolidColorShader( gl );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferBasic( gl, shader, mesh );

        //
        // MATERIAL
        //
        material = new XGLSolidColorMaterial( gl, meshMaterial.emissiveColor );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform3f( shader.program.uniformHandles['solidColor'], material.color.r, material.color.g, material.color.b );
    }

    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = mesh.transformMatrix.clone();
        transform.append( scene.projection );
        gl.uniformMatrix4fv( shader.program.uniformHandles['transformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );
    }
    
    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawArrays( GL.TRIANGLES, 0, mesh.numberOfPrimitives );
    }
}
