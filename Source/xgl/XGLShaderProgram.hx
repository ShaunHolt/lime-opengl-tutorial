package xgl;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLUniformLocation;


class XGLShaderProgram {

    public var attributes : Map< String, Int > = [];
    public var uniforms : Map< String, GLUniformLocation > = [];

    public var program : GLProgram;

    public function new( gl : WebGLRenderContext, vertexSource : String, fragmentSource : String, attributeNames : Array< String >, uniformNames : Array< String > ) {

	    program = GLProgram.fromSources( gl, vertexSource, fragmentSource );

	    for( attrName in attributeNames ) {

		    attributes[ attrName ] = gl.getAttribLocation( program, attrName );
		}

	    for( uniName in uniformNames ) {

		    uniforms[ uniName ] = gl.getUniformLocation( program, uniName );
		}
	}
}
