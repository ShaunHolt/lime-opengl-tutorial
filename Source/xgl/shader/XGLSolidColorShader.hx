package xgl.shader;

import lime.graphics.WebGLRenderContext;

import xgl.engine.XGLShaderProgram;

import xgl.engine.XGLShader;


class XGLSolidColorShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

        var vertexShaderSourceA = "
            // #version 120
            attribute vec3 position;

			uniform mat4 transformMatrix;

            void main (void) {
                gl_Position = transformMatrix * vec4(position, 1.0);
            }
        ";

        var fragmentShaderSourceA = 
            #if (!desktop || rpi)
            "precision mediump float;" +
            #end
            "
            // #version 120
            uniform vec3 solidColor;

            void main (void) {
                gl_FragColor = vec4(solidColor, 1.0);
            }
        ";

        program = new XGLShaderProgram( gl, vertexShaderSourceA, fragmentShaderSourceA, [ "position" ], [ "transformMatrix", "solidColor" ] );
    }
}
