package xgl.geometry;

import lime.graphics.WebGLRenderContext;

import lime.utils.Float32Array;

import xgl.XGLArrayBuffer;

import xgl.shader.XGLShader;


class XGLGeometryBuffer {

	public var buffer : XGLArrayBuffer;
    public var shader : XGLShader;

    public function new( gl : WebGLRenderContext, shader : XGLShader ) {

        this.shader = shader;
    }

    public function describeProgramAttributes( gl : WebGLRenderContext ) : Void { }
}
