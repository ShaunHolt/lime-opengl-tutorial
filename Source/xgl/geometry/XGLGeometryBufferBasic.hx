package xgl.geometry;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.utils.Float32Array;

import xgl.engine.XGLArrayBuffer;

import xgl.engine.XGLGeometryBuffer;

import xgl.engine.XGLShader;

import core.Mesh;


class XGLGeometryBufferBasic extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, mesh : Mesh ) {

        super( gl, shader );

        // attribute format:
        // X, Y, Z -> number of elements == 5
		buffer = new XGLArrayBuffer( gl, createFromMesh( mesh ), 3 );
        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 * Float32Array.BYTES_PER_ELEMENT );
    }

    private function createFromMesh( mesh : Mesh ) : Float32Array {

        var array = [];

        for( vertex in mesh.vertices ) {

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
        }

        return new Float32Array( array );
    }

    override public function describeProgramAttributes( gl : WebGLRenderContext ) : Void {

        gl.vertexAttribPointer(
            // attribute handle (ID)
            shader.program.attributeHandles['position'],
            // the number of components per generic vertex attribute. Must be 1, 2, 3, 4. 
            buffer.attributeParams['position'].componentCount,
            // the data type of each component in the array
            // The symbolic constants GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, and GL_UNSIGNED_INT are accepted by glVertexAttribPointer and glVertexAttribIPointer
            GL.FLOAT,
            // normalized or not
            false,
            // the byte offset between consecutive generic vertex attributes.
            buffer.vertexAttributesSize,
            // an offset of the first component of the first generic vertex attribute in the array buffer
            // "position" attribute starts right at the start - 0
            buffer.attributeParams['position'].attributeOffset
        );
    }
}
