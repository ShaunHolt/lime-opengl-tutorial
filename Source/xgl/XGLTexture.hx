package xgl;

import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;
import lime.graphics.PixelFormat;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLTexture;


class XGLTexture {

    public var textureUnit : Int = 1;
    public var textureHandle : GLTexture;

    public function new( gl : WebGLRenderContext, image : Image ) {

	    textureHandle = gl.createTexture();

	    uploadTextureBuffer( gl, image );

	}

    private function uploadTextureBuffer( gl : WebGLRenderContext, image : Image ) : Void {

        // no mipmap for this example
        var mipMap = false;

        var imageWidth = image.width;
        var imageHeight = image.height;

        if( image.format != PixelFormat.RGBA32 ) {
            // this converts image to RGBA32
            image.format = PixelFormat.RGBA32;
        }

        // By default, glBindTexture binds our texture to texture unit 0
        // Just for the sake of clarity, we will explicitly set the texture unit to use, unit 0
        // this texture unit will be bound to the first declared sampler2D uniform in your fragment shader.
        gl.activeTexture( GL.TEXTURE0 + textureUnit );
	    gl.bindTexture( GL.TEXTURE_2D, textureHandle );

        // when scaling down
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR );
        // when scaling up
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR );
		// gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
		// gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE );
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE );

        // This effectively uploads the texture data (pixels) to GPU
        gl.texImage2D(
            GL.TEXTURE_2D, 
            0, // base image level. if > 0 it references n-th mipmap reduction level
            GL.RGBA, // 'internalFormat': ask OpenGL how to store texture internally. We asked for an array of uncompressed RGBA 32-bit values
            imageWidth, 
            imageHeight, 
            0, // border width: 0 or 1.
            GL.RGBA, // 'format': format for one pixel in our texture data we are uploading to GPU
            GL.UNSIGNED_BYTE, // 'type': type of one element of one pixel data. We said each one of R, G, B and A components is represented by an unsigned byte value.
            image.data // actual data of the image we want to upload to GPU (lime's UInt8Array)
        );

        // generate mipmaps AFTER image data has been loaded to GPU
        if( mipMap ) {
            // we will use OpenGL to generate mipmap levels
            // we could have specified each level ourselves by calling
            // gl.texImage2D() for each level and specifying the level as second parameter
            gl.generateMipmap( GL.TEXTURE_2D );
        }

        // when done, unbind
	    gl.bindTexture( GL.TEXTURE_2D, null );
	}
}
