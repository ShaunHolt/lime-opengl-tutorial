package xgl;

import lime.graphics.WebGLRenderContext;

import lime.math.RGBA;

import xgl.XGLColor;
import xgl.XGLShaderProgram;


class XGLRenderObject {

	public var shaderProgram : XGLShaderProgram;

	public function new( gl : WebGLRenderContext ) {

		initShaderProgram( gl );
		initGeometry( gl );
		initMaterial( gl );
	}

	private function initShaderProgram( gl : WebGLRenderContext ) : Void { }

	private function initGeometry( gl : WebGLRenderContext ) : Void { }

	private function initMaterial( gl : WebGLRenderContext ) : Void { }

	public function render( gl : WebGLRenderContext ) : Void {

		//
		// --- SETUP
		//

		//
		// ACTIVATE PROGRAM
		//
		gl.useProgram( shaderProgram.program );

		//
		// Enable PROGRAM attributes
		//
		for( attr in shaderProgram.attributes ) {
			gl.enableVertexAttribArray( attr );
		}


		//
		// --- RENDER
		//
		renderSetupUniforms( gl );
		renderSetupTextures( gl );
		renderSetupAttributes( gl );
		renderDraw( gl );



		//
		// --- CLEANUP
		//

		//
		// Disable PROGRAM attributes
		//
		for( attr in shaderProgram.attributes ) {
			gl.disableVertexAttribArray( attr );
		}
	}

	private function renderSetupUniforms( gl : WebGLRenderContext ) : Void { }
	
	private function renderSetupTextures( gl : WebGLRenderContext ) : Void { }
	
	private function renderSetupAttributes( gl : WebGLRenderContext ) : Void { }
	
	private function renderDraw( gl : WebGLRenderContext ) : Void { }
}
